package me.sharkz.languagechat.translations;

import me.sharkz.milkalib.translations.ITranslation;
import me.sharkz.milkalib.translations.Translation;
import me.sharkz.milkalib.translations.TranslationsManager;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

public enum LcEn implements ITranslation {
    ALREADY_EXISTS("&c&l[!] &eA chat channel with this name already exists!"),
    CREATED("&a&l[!] &7You have &a&lcreated&7 a new chat channel with name &b&l%name%&7!"),
    UNKNOWN_CHANNEL("&c&l[!] &eCouldn't find any chat channel with this name!"),
    DELETED("&a&l[!] &7You have &c&ldeleted&7 chat channel with name &b&l%name%&7!"),
    RENAMED("&a&l[!] &7You have renamed channel &b&l%oldName%&7 to &a&l%newName%&7!"),
    CHANNEL_JOINED("&f&l[!] &7You have &a&ljoined&7 the &b&l%name%&7 chat channel!"),
    SPY_ENABLE("&f&l[!] &7You have &a&lenabled&7 the spy mode."),
    SPY_DISABLED("&f&l[!] &7You have &c&ldisabled&7 the spy mode."),
    PERMISSION_DEFINED("&a&l[!] &7You have defined a permission for the chat channel &a&l%name%&7!"),
    PREFIX_DEFINED("&a&l[!] &7You have change the prefix of %name% channel to %prefix%!"),
    ;

    private final String content;
    private TranslationsManager manager;

    LcEn(String content) {
        this.content = content;
    }

    @Override
    public Locale getLanguage() {
        return new Locale("en", "US");
    }

    @Override
    public Translation get() {
        return new Translation(this, this.name(), this.content);
    }

    @Override
    public List<Translation> translations() {
        return Arrays.stream(values()).map(LcEn::get).collect(Collectors.toList());
    }

    @Override
    public void setManager(TranslationsManager manager) {
        this.manager = manager;
    }

    @Override
    public TranslationsManager getManager() {
        return manager;
    }


    @Override
    public String toString() {
        return this.get().translate();
    }
}

