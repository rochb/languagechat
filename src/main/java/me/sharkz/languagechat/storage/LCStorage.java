package me.sharkz.languagechat.storage;

import me.sharkz.languagechat.channels.ChatChannel;
import me.sharkz.languagechat.configurations.Configuration;
import me.sharkz.languagechat.utils.StorageUtils;
import me.sharkz.milkalib.storage.StorageManager;
import me.sharkz.milkalib.utils.logger.MilkaLogger;
import me.sharkz.milkalib.utils.sql.Column;
import me.sharkz.milkalib.utils.sql.DataTypes;
import me.sharkz.milkalib.utils.sql.QueryBuilder;
import org.bukkit.OfflinePlayer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class LCStorage extends StorageManager {

    private String CHANNELS_TABLE;

    public LCStorage(Configuration config) {
        super(config.getStorage());
    }

    @Override
    protected void createDefaultTables() {
        CHANNELS_TABLE = getTableName("channels");

        getDatabase().query(new QueryBuilder(CHANNELS_TABLE).createTableIfNotExists()
                .column(Column.dataType("id", DataTypes.Limit.VARCHAR, 100))
                .column(Column.dataType("name", DataTypes.Limit.VARCHAR, 100))
                .column(Column.dataType("prefix", DataTypes.Limit.VARCHAR, 100))
                .column(Column.dataType("permission", DataTypes.Limit.VARCHAR, 200))
                .column(Column.dataType("members", DataTypes.TEXT))
                .primaryKey("id")
                .build());
    }

    public void insert(ChatChannel channel) {
        getDatabase().asyncQuery(new QueryBuilder(CHANNELS_TABLE).insert()
                .insert("id", channel.getId().toString())
                .insert("name", channel.getName())
                .insert("members", StorageUtils.deserialize(channel.getMembers()))
                .insert("prefix", channel.getPrefix())
                .insert("permission", channel.getPermission())
                .build());
    }

    public void update(ChatChannel channel) {
        getDatabase().asyncQuery(new QueryBuilder(CHANNELS_TABLE).update()
                .set("name", channel.getName())
                .set("members", StorageUtils.deserialize(channel.getMembers()))
                .set("prefix", channel.getPrefix())
                .set("permission", channel.getPermission())
                .toWhere()
                .where("id", channel.getId().toString())
                .build());
    }

    public void delete(ChatChannel channel) {
        getDatabase().asyncQuery(new QueryBuilder(CHANNELS_TABLE).delete()
                .where("id", channel.getId().toString())
                .build());
    }

    public List<ChatChannel> get() {
        List<ChatChannel> channels = new ArrayList<>();
        ResultSet rs = getDatabase().executeQuery(new QueryBuilder(CHANNELS_TABLE).select().buildAllColumns());
        try {
            while (rs.next()) {
                UUID id = UUID.fromString(rs.getString("id"));
                String name = rs.getString("name");
                String permission = rs.getString("permission");
                String prefix = rs.getString("prefix");
                List<OfflinePlayer> members = StorageUtils.serialize(rs.getString("members"));
                channels.add(new ChatChannel(id, name, prefix, permission, members));
            }
        } catch (SQLException e) {
            MilkaLogger.warn("Couldn't get chat channels from database: ");
            e.printStackTrace();
        }
        return channels;
    }
}
