package me.sharkz.languagechat.utils;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;

import java.util.*;
import java.util.stream.Collectors;

public class StorageUtils {

    public static String deserialize(List<OfflinePlayer> members) {
        StringBuilder b = new StringBuilder();
        members.forEach(player -> b.append(player.getUniqueId().toString()).append(";"));
        return b.toString();
    }

    public static List<OfflinePlayer> serialize(String data) {
        return Arrays.stream(data.split(";"))
                .map(UUID::fromString)
                .map(Bukkit::getOfflinePlayer)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }
}
