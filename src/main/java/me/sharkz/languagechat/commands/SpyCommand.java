package me.sharkz.languagechat.commands;

import me.sharkz.languagechat.channels.ChannelsManager;
import me.sharkz.languagechat.translations.LcEn;
import me.sharkz.milkalib.MilkaPlugin;
import me.sharkz.milkalib.commands.CommandResult;
import me.sharkz.milkalib.commands.MilkaCommand;

public class SpyCommand extends MilkaCommand {

    private final ChannelsManager manager;

    public SpyCommand(ChannelsManager manager) {
        this.manager = manager;
        setConsoleCanUse(false);
        setDescription("Spy all chat channels.");
        setPermission("languagechat.spy");
    }

    @Override
    public CommandResult perform(MilkaPlugin milkaPlugin, String[] strings) {
        if (manager.toggleSpy(player))
            message(sender, LcEn.SPY_ENABLE.name(), LcEn.SPY_ENABLE.toString());
        else
            message(sender, LcEn.SPY_DISABLED.name(), LcEn.SPY_DISABLED.toString());
        return CommandResult.COMPLETED;
    }
}
