package me.sharkz.languagechat.commands;

import com.google.common.collect.ImmutableMap;
import me.sharkz.languagechat.channels.ChannelsManager;
import me.sharkz.languagechat.translations.LcEn;
import me.sharkz.milkalib.MilkaPlugin;
import me.sharkz.milkalib.commands.CommandResult;
import me.sharkz.milkalib.commands.MilkaCommand;

public class CreateChannelCommand extends MilkaCommand {

    private final ChannelsManager manager;

    public CreateChannelCommand(ChannelsManager manager) {
        this.manager = manager;
        addSubCommand("create", "add");
        setDescription("Create new chat channel.");
        setPermission("languagechat.create");
        addRequiredArg("name");
    }

    @Override
    public CommandResult perform(MilkaPlugin milkaPlugin, String[] strings) {
        if (manager.getByName(argAsString(0)).isPresent()) {
            message(sender, LcEn.ALREADY_EXISTS.name(), LcEn.ALREADY_EXISTS.toString());
            return CommandResult.COMPLETED;
        }
        manager.create(argAsString(0));
        message(sender, LcEn.CREATED.name(), LcEn.CREATED.toString(), ImmutableMap.of("%name%", argAsString(0)));
        return CommandResult.COMPLETED;
    }
}
