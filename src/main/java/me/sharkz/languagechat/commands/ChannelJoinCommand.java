package me.sharkz.languagechat.commands;

import com.google.common.collect.ImmutableMap;
import me.sharkz.languagechat.channels.ChannelsManager;
import me.sharkz.languagechat.channels.ChatChannel;
import me.sharkz.languagechat.translations.LcEn;
import me.sharkz.milkalib.MilkaPlugin;
import me.sharkz.milkalib.commands.CommandResult;
import me.sharkz.milkalib.commands.MilkaCommand;
import me.sharkz.milkalib.translations.en.CommandsEn;

public class ChannelJoinCommand extends MilkaCommand {

    private final ChannelsManager manager;
    private final ChatChannel channel;

    public ChannelJoinCommand(ChannelsManager manager, ChatChannel channel) {
        this.manager = manager;
        this.channel = channel;
        setDescription("Join " + channel.getName() + " chat channel.");
        setConsoleCanUse(false);
    }

    @Override
    public CommandResult perform(MilkaPlugin milkaPlugin, String[] strings) {
        if (channel.getPermission() != null && !player.hasPermission(channel.getPermission())) {
            message(player, CommandsEn.NO_PERMISSION.name(), CommandsEn.NO_PERMISSION.toString());
            return CommandResult.COMPLETED;
        }
        manager.setPlayerChannel(player, channel);
        message(player, LcEn.CHANNEL_JOINED.name(), LcEn.CHANNEL_JOINED.toString(), ImmutableMap.of("%name%", channel.getName()));
        return CommandResult.COMPLETED;
    }
}
