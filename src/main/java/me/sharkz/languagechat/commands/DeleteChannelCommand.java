package me.sharkz.languagechat.commands;

import com.google.common.collect.ImmutableMap;
import me.sharkz.languagechat.channels.ChannelsManager;
import me.sharkz.languagechat.channels.ChatChannel;
import me.sharkz.languagechat.translations.LcEn;
import me.sharkz.milkalib.MilkaPlugin;
import me.sharkz.milkalib.commands.CommandResult;
import me.sharkz.milkalib.commands.MilkaCommand;

import java.util.Optional;

public class DeleteChannelCommand extends MilkaCommand {

    private final ChannelsManager manager;

    public DeleteChannelCommand(ChannelsManager manager) {
        this.manager = manager;
        setDescription("Delete an existing chat channel.");
        setPermission("languagechat.delete");
        addSubCommand("delete", "remove");
        addRequiredArg("name");
    }

    @Override
    public CommandResult perform(MilkaPlugin milkaPlugin, String[] strings) {
        Optional<ChatChannel> channel = manager.getByName(argAsString(0));
        if (!channel.isPresent()) {
            message(sender, LcEn.UNKNOWN_CHANNEL.name(), LcEn.UNKNOWN_CHANNEL.toString());
            return CommandResult.COMPLETED;
        }
        manager.delete(channel.get());
        message(sender, LcEn.DELETED.name(), LcEn.DELETED.toString(), ImmutableMap.of("%name%", channel.get().getName()));
        return CommandResult.COMPLETED;
    }
}
