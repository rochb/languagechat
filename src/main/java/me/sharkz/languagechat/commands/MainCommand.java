package me.sharkz.languagechat.commands;

import me.sharkz.languagechat.LC;
import me.sharkz.languagechat.channels.ChannelsManager;
import me.sharkz.milkalib.MilkaPlugin;
import me.sharkz.milkalib.commands.CommandResult;
import me.sharkz.milkalib.commands.MilkaCommand;
import me.sharkz.milkalib.commands.defaults.HelpCommand;
import me.sharkz.milkalib.commands.defaults.VersionCommand;

public class MainCommand extends MilkaCommand {

    public MainCommand(LC plugin, ChannelsManager manager) {
        setDescription("Main command of the plugin");

        addSubCommand(new CreateChannelCommand(manager));
        addSubCommand(new DeleteChannelCommand(manager));
        addSubCommand(new RenameChannelCommand(manager));
        addSubCommand(new SetPermissionCommand(manager));
        addSubCommand(new SetPrefixCommand(manager));

        /* Defaults */
        addSubCommand(new HelpCommand(plugin));
        addSubCommand(new VersionCommand());
    }

    @Override
    public CommandResult perform(MilkaPlugin milkaPlugin, String[] strings) {
        HelpCommand.getInstance().sendHelpMenu(sender, 1);
        return CommandResult.COMPLETED;
    }
}
