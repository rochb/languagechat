package me.sharkz.languagechat.commands;

import com.google.common.collect.ImmutableMap;
import me.sharkz.languagechat.channels.ChannelsManager;
import me.sharkz.languagechat.channels.ChatChannel;
import me.sharkz.languagechat.translations.LcEn;
import me.sharkz.milkalib.MilkaPlugin;
import me.sharkz.milkalib.commands.CommandResult;
import me.sharkz.milkalib.commands.MilkaCommand;

import java.util.Optional;

public class RenameChannelCommand extends MilkaCommand {

    private final ChannelsManager manager;

    public RenameChannelCommand(ChannelsManager manager) {
        this.manager = manager;
        setDescription("Rename an existing chat channel.");
        setPermission("languagechat.rename");
        addSubCommand("rename");
        addRequiredArg("current name");
        addRequiredArg("new name");
    }

    @Override
    public CommandResult perform(MilkaPlugin milkaPlugin, String[] strings) {
        Optional<ChatChannel> channel = manager.getByName(argAsString(0));
        if (!channel.isPresent()) {
            message(sender, LcEn.UNKNOWN_CHANNEL.name(), LcEn.UNKNOWN_CHANNEL.toString());
            return CommandResult.COMPLETED;
        }else if(manager.getByName(argAsString(1)).isPresent()){
            message(sender, LcEn.ALREADY_EXISTS.name(), LcEn.ALREADY_EXISTS.toString());
            return CommandResult.COMPLETED;
        }
        manager.rename(channel.get(), argAsString(1));
        message(sender, LcEn.RENAMED.name(), LcEn.RENAMED.toString(), ImmutableMap.of("%oldName%", argAsString(0), "%newName%", argAsString(1)));
        return CommandResult.COMPLETED;
    }
}
