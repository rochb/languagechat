package me.sharkz.languagechat.commands;

import com.google.common.collect.ImmutableMap;
import me.sharkz.languagechat.channels.ChannelsManager;
import me.sharkz.languagechat.channels.ChatChannel;
import me.sharkz.languagechat.translations.LcEn;
import me.sharkz.milkalib.MilkaPlugin;
import me.sharkz.milkalib.commands.CommandResult;
import me.sharkz.milkalib.commands.MilkaCommand;

import java.util.Optional;

public class SetPrefixCommand extends MilkaCommand {

    private final ChannelsManager manager;

    public SetPrefixCommand(ChannelsManager manager) {
        this.manager = manager;
        setPermission("languagechat.setprefix");
        setDescription("Allow you to change the prefix of a chat channel.");
        addRequiredArg("channel");
        addRequiredArg("prefix");
        addSubCommand("setPrefix");
    }

    @Override
    public CommandResult perform(MilkaPlugin milkaPlugin, String[] strings) {
        Optional<ChatChannel> channel = manager.getByName(argAsString(0));
        if (!channel.isPresent()) {
            message(sender, LcEn.UNKNOWN_CHANNEL.name(), LcEn.UNKNOWN_CHANNEL.toString());
            return CommandResult.COMPLETED;
        }
        channel.ifPresent(channel1 -> {
            channel1.setPrefix(argAsString(1));
            manager.update(channel1);
            message(sender, LcEn.PREFIX_DEFINED.name(), LcEn.PREFIX_DEFINED.toString(), ImmutableMap.of("%name%", channel1.getName(), "%prefix%", argAsString(1)));
        });
        return CommandResult.COMPLETED;
    }
}
