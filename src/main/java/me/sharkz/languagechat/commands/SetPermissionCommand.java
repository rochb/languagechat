package me.sharkz.languagechat.commands;

import com.google.common.collect.ImmutableMap;
import me.sharkz.languagechat.channels.ChannelsManager;
import me.sharkz.languagechat.channels.ChatChannel;
import me.sharkz.languagechat.translations.LcEn;
import me.sharkz.milkalib.MilkaPlugin;
import me.sharkz.milkalib.commands.CommandResult;
import me.sharkz.milkalib.commands.MilkaCommand;

import java.util.Optional;

public class SetPermissionCommand extends MilkaCommand {

    private final ChannelsManager manager;

    public SetPermissionCommand(ChannelsManager manager) {
        this.manager = manager;
        setDescription("Set channel join permission.");
        setPermission("languagechat.setpermission");
        addRequiredArg("channel");
        addRequiredArg("permission");
        addSubCommand("setPermission");
    }

    @Override
    public CommandResult perform(MilkaPlugin milkaPlugin, String[] strings) {
        Optional<ChatChannel> channel = manager.getByName(argAsString(0));
        if (!channel.isPresent()) {
            message(sender, LcEn.UNKNOWN_CHANNEL.name(), LcEn.UNKNOWN_CHANNEL.toString());
            return CommandResult.COMPLETED;
        }
        channel.ifPresent(channel1 -> {
            channel1.setPermission(argAsString(1));
            manager.update(channel1);
            message(sender, LcEn.PERMISSION_DEFINED.name(), LcEn.PERMISSION_DEFINED.toString(), ImmutableMap.of("%name%", channel1.getName()));
        });
        return CommandResult.COMPLETED;
    }
}
