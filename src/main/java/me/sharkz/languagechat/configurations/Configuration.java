package me.sharkz.languagechat.configurations;

import me.sharkz.milkalib.MilkaPlugin;
import me.sharkz.milkalib.configuration.Configurable;
import me.sharkz.milkalib.configuration.wrapper.ConfigurableFileWrapper;
import org.bukkit.configuration.ConfigurationSection;

import java.io.File;

public class Configuration extends ConfigurableFileWrapper {

    @Configurable(key = "language")
    private String language;

    @Configurable(key = "storage")
    private ConfigurationSection storage;

    public Configuration(MilkaPlugin plugin) {
        super(new File(plugin.getDataFolder(), "config.yml"), plugin.getClass().getClassLoader().getResource("config.yml"));
    }

    public String getLanguage() {
        return language;
    }

    public ConfigurationSection getStorage() {
        return storage;
    }
}
