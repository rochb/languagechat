package me.sharkz.languagechat.listeners;

import me.sharkz.languagechat.channels.ChannelsManager;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import java.util.concurrent.atomic.AtomicBoolean;

public class ChatListener implements Listener {

    private final ChannelsManager manager;

    public ChatListener(ChannelsManager manager) {
        this.manager = manager;
    }

    @EventHandler
    public void onChat(AsyncPlayerChatEvent e) {
        manager.getPlayerChannel(e.getPlayer())
                .ifPresent(channel -> e.setFormat(ChatColor.GRAY + "[" + ChatColor.AQUA + channel.getPrefix() + ChatColor.GRAY + "]" + e.getFormat()));
        e.getRecipients()
                .removeIf(player -> {
                    AtomicBoolean remove = new AtomicBoolean(false);
                    manager.getPlayerChannel(e.getPlayer())
                            .ifPresent(channel -> remove.set((channel.getMembers().contains(player) || manager.isSpying(player) || player.equals(e.getPlayer()))));
                    return !remove.get();
                });
    }
}
