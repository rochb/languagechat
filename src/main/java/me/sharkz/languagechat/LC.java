package me.sharkz.languagechat;

import me.sharkz.languagechat.channels.ChannelsManager;
import me.sharkz.languagechat.commands.MainCommand;
import me.sharkz.languagechat.commands.SpyCommand;
import me.sharkz.languagechat.configurations.Configuration;
import me.sharkz.languagechat.listeners.ChatListener;
import me.sharkz.languagechat.storage.LCStorage;
import me.sharkz.languagechat.translations.LcEn;
import me.sharkz.milkalib.MilkaPlugin;
import me.sharkz.milkalib.hooks.PapiHook;
import me.sharkz.milkalib.hooks.VaultHook;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Objects;

public final class LC extends MilkaPlugin {

    private Configuration config;
    private LCStorage storage;

    @Override
    public void onEnable() {
        /* Initialize */
        init(this);

        /* Header */
        sendHeader("Sharkz");

        /* Dependencies */
        initDependencies();

        /* Hooks */
        initHooks();
        registerHook(new PapiHook());

        /* Translations */
        registerTranslation(LcEn.class);

        /* Configurations */
        initConfigurations();
        config = (Configuration) registerConfig(new Configuration(this));

        /* Storage */
        storage = new LCStorage(config);

        /* Channels */
        ChannelsManager manager = new ChannelsManager(storage);

        /* Commands */
        initCommands();
        registerCommand("languagechat", new MainCommand(this, manager), "lchat");
        registerCommand("spychat", new SpyCommand(manager));
        manager.registerCommands(getCommandManager());

        /* Listeners */
        registerListener(new ChatListener(manager));

        /* Footer */
        sendFooter();
    }

    @Override
    public void onDisable() {
        /* Storage */
        Objects.requireNonNull(storage).getDatabase().closeConnection();
    }

    @Override
    protected boolean isPaid() {
        return true;
    }

    @Override
    protected int getConfigurationVersion() {
        return 1;
    }

    @Override
    public String getPrefix() {
        return "&b&lLanguage&9&lChat";
    }

    @Override
    public String getLanguage() {
        String l = config.getLanguage();
        if (l != null) return l;
        return getConfig().getString("language", "en_US");
    }
}
