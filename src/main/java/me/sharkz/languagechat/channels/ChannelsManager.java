package me.sharkz.languagechat.channels;

import me.sharkz.languagechat.commands.ChannelJoinCommand;
import me.sharkz.languagechat.storage.LCStorage;
import me.sharkz.milkalib.commands.CommandManager;
import org.bukkit.OfflinePlayer;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ChannelsManager {

    private final LCStorage storage;
    private final List<ChatChannel> channels;
    private CommandManager commandManager;

    private final List<OfflinePlayer> spy = new ArrayList<>();

    public ChannelsManager(LCStorage storage) {
        this.storage = storage;
        this.channels = storage.get();
    }

    public void registerCommands(CommandManager manager) {
        this.commandManager = manager;
        channels.forEach(channel -> manager.registerCommand(channel.getName(), new ChannelJoinCommand(this, channel)));
    }

    public boolean isSpying(OfflinePlayer player) {
        return spy.contains(player);
    }

    public boolean toggleSpy(OfflinePlayer player) {
        if (isSpying(player)) spy.remove(player);
        else spy.add(player);
        return isSpying(player);
    }

    public void setPlayerChannel(OfflinePlayer player, ChatChannel channel) {
        getPlayerChannel(player).ifPresent(channel1 -> {
            channel1.removeMember(player);
            storage.update(channel1);
        });
        channel.addMember(player);
        storage.update(channel);
    }

    public void rename(ChatChannel channel, String name) {
        channel.setName(name);
        update(channel);
    }

    public void update(ChatChannel channel){
        storage.update(channel);
    }

    public ChatChannel create(String name) {
        ChatChannel channel = new ChatChannel(name);
        storage.insert(channel);
        channels.add(channel);
        commandManager.registerCommand(channel.getName().toLowerCase(), new ChannelJoinCommand(this, channel));
        return channel;
    }

    public void delete(ChatChannel channel) {
        channels.remove(channel);
        storage.delete(channel);
    }

    public Optional<ChatChannel> getPlayerChannel(OfflinePlayer player) {
        return channels.stream().filter(channel -> channel.isMember(player)).findFirst();
    }

    public Optional<ChatChannel> getByName(String name) {
        return channels.stream().filter(channel -> channel.getName().equalsIgnoreCase(name)).findFirst();
    }

    public List<ChatChannel> getChannels() {
        return channels;
    }
}
