package me.sharkz.languagechat.channels;

import org.bukkit.OfflinePlayer;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ChatChannel {

    private final UUID id;
    private String name;
    private String prefix;
    private String permission;
    private final List<OfflinePlayer> members;

    public ChatChannel(String name) {
        this.id = UUID.randomUUID();
        this.name = name;
        this.prefix = name.substring(0, 2).toUpperCase();
        this.permission = "none";
        this.members = new ArrayList<>();
    }

    public ChatChannel(String name, String prefix) {
        this.id = UUID.randomUUID();
        this.name = name;
        this.prefix = prefix;
        this.permission = "none";
        this.members = new ArrayList<>();
    }

    public ChatChannel(UUID id, String name, String prefix, String permission, List<OfflinePlayer> members) {
        this.id = id;
        this.name = name;
        this.prefix = prefix;
        this.permission = permission;
        this.members = members;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public void addMember(OfflinePlayer player) {
        members.add(player);
    }

    public void removeMember(OfflinePlayer player) {
        members.remove(player);
    }

    public boolean isMember(OfflinePlayer player) {
        return members.contains(player);
    }

    /* Getters */
    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPrefix() {
        return prefix;
    }

    public String getPermission() {
        if (permission.equalsIgnoreCase("none")) return null;
        return permission;
    }

    public List<OfflinePlayer> getMembers() {
        return members;
    }
}
